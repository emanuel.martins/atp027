import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app-root/app.component';

import { AppRoutingModule } from './app-routing.module';
import { ProductsRegisterComponent } from './components/products-register/products-register.component';
import { ProductsListComponent } from './components/products-list/products-list.component';

@NgModule({
  declarations: [AppComponent, ProductsRegisterComponent, ProductsListComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
